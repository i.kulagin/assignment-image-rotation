#ifndef IMAGE_H
#define IMAGE_H

#include <stdint.h>
#include <stdlib.h>

struct image {
    uint64_t width, height;
    struct pixel* pixels;
};

struct pixel {
    uint8_t b, g, r;
};

void image_create(struct image* img, size_t width, size_t height);

void image_destroy(const struct image* img);

#endif
