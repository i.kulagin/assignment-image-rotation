#include "transform.h"

struct image rotate(struct image const* source) {
    struct image result = {0};
    image_create(&result, source->height, source->width);
    for (uint64_t i = 0; i < source->width; i++) {
        for (uint64_t j = 0; j < source->height; j++) {  
            result.pixels[i * source->height + j] = source->pixels[(source->height - j - 1) * source->width + i];
        }
    }
    return result;
}
