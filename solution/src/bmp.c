#include <stdio.h>

#include "image.h"
#include "bmp.h"
#include "transform.h"

uint8_t get_padding(uint64_t width) {
    return width % 4;
}

static enum read_status read_header(FILE* in, struct bmp_header* header) {
    if (fread(header, sizeof(struct bmp_header), 1, in) != 1) {
        return READ_INVALID_HEADER;
    }
    return READ_OK;
}

static enum read_status read_image(FILE* in, struct image* img) {
    for (size_t i = 0; i < img->height; i++) {
        if (fread(img->pixels + i * img->width, sizeof(struct pixel), img->width, in) != img->width) {
            return READ_INVALID_IMAGE;
        }
        if (fseek(in, get_padding(img->width), SEEK_CUR) != 0) {
            return READ_INVALID_IMAGE;
        }
    }
    return READ_OK;
}

enum read_status from_bmp(FILE* in, struct image* img) {
    struct bmp_header header = {0};
    enum read_status header_status = read_header(in, &header);
    if (header_status != READ_OK) {
        return header_status;
    }
    image_create(img, header.biWidth, header.biHeight);
    enum read_status content_status = read_image(in, img);
    if (content_status != READ_OK) {
        return content_status;
    }
    return READ_OK;
}

enum write_status write_header(FILE* out, struct image const* img) {
    const uint32_t img_size = (sizeof(struct pixel) * img->width + get_padding(img->width)) * img->height;
    struct bmp_header header = {
            .bfType = 19778,
            .bfileSize = sizeof(struct bmp_header) + img_size,
            .bfReserved = 0,
            .bOffBits = sizeof(struct bmp_header),
            .biSize = 40,
            .biWidth = img->width,
            .biHeight = img->height,
            .biPlanes = 1,
            .biBitCount = 24,
            .biCompression = 0,
            .biSizeImage = img_size,
            .biXPelsPerMeter = 2834,
            .biYPelsPerMeter = 2834,
            .biClrUsed = 0,
            .biClrImportant = 0,
    };
    if (fwrite(&header, sizeof(struct bmp_header), 1, out) != 1) {
        return WRITE_ERROR;
    }
    return WRITE_OK;
}

enum write_status write_image(struct image const* img, FILE* out, const uint8_t padding) {
    const uint64_t zero = 0;
    uint64_t count = 0;
    for (size_t i = 0; i < img->height; ++i) {
        size_t const write_result = fwrite(img->pixels + i * img->width, sizeof(struct pixel), img->width, out);
        if (write_result != img->width) {
            return WRITE_ERROR;
        }
        count += (uint64_t) write_result;
        size_t const write_result_zero = fwrite(&zero, 1, padding, out);
        if (write_result_zero != padding) {
            return WRITE_ERROR;
        }
    }
    if (count != img->height * img->width) 
        return WRITE_ERROR;
    return WRITE_OK;
}

enum write_status to_bmp(FILE* out, struct image const* img) {
    enum write_status header_status = write_header(out, img);
    if (header_status != WRITE_OK) {
        return header_status;
    }
    return write_image(img, out, get_padding(img->width));
}

