#include "image.h"

void image_create(struct image* img, uint64_t width, uint64_t height) {
    img->width =width;
    img->height = height;
    img->pixels = malloc(width * height * sizeof(struct pixel));
}

void image_destroy(const struct image *img) {
    free(img->pixels);
}
