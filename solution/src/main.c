#include <stdio.h>

#include "bmp.h"
#include "image.h"
#include "transform.h"

int main(int argc, char** argv) {
    (void) argc; (void) argv;
    if (argc != 3) {
        printf("Wrong number of arguments");
        return 0;
    }

    FILE* in = fopen(argv[1], "rb");
    if (in == NULL){
        printf("Can't open input file");
        return 0;
    }

    struct image input_image = {0};

    if (from_bmp(in, &input_image) != READ_OK) {
        printf("Can't read input file");
        image_destroy(&input_image);
        if (fclose(in)) {
            printf("Can't close input file");
            return 0;
        }
        return 0;
    }

    if (fclose(in)) {
            printf("Can't close input file");
            return 0;
        }

    FILE* out = fopen(argv[2], "wb");
    if (out == NULL){
        printf("Can't open output file");
        return 0;
    }

    struct image output_image = rotate(&input_image);
    image_destroy(&input_image);
    if (to_bmp(out, &output_image) != WRITE_OK) {
        image_destroy(&output_image);
        printf("Can't write to the output file");
        if (fclose(out)) {
            printf("Can't close output file");
            return 0;
        }
        return 0;
    }

    if (fclose(out)) {
            printf("Can't close output file");
            return 0;
        }

    image_destroy(&output_image);
    return 0;
}
